
BasicGame.Game = function (game) {

    //  When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;      //  a reference to the currently running game (Phaser.Game)
    this.add;       //  used to add sprites, text, groups, etc (Phaser.GameObjectFactory)
    this.camera;    //  a reference to the game camera (Phaser.Camera)
    this.cache;     //  the game cache (Phaser.Cache)
    this.input;     //  the global input manager. You can access this.input.keyboard, this.input.mouse, as well from it. (Phaser.Input)
    this.load;      //  for preloading assets (Phaser.Loader)
    this.math;      //  lots of useful common math operations (Phaser.Math)
    this.sound;     //  the sound manager - add a sound, play one, set-up markers, etc (Phaser.SoundManager)
    this.stage;     //  the game stage (Phaser.Stage)
    this.time;      //  the clock (Phaser.Time)
    this.tweens;    //  the tween manager (Phaser.TweenManager)
    this.state;     //  the state manager (Phaser.StateManager)
    this.world;     //  the game world (Phaser.World)
    this.particles; //  the particle manager (Phaser.Particles)
    this.physics;   //  the physics manager (Phaser.Physics)
    this.rnd;       //  the repeatable random number generator (Phaser.RandomDataGenerator)

    //  You can use any of these from any function within this State.
    //  But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

    this.dialogue = ["Click to go to the next sentence.", 
        "This is created using PhaserJS.", 
        "Now I can make a VN using Phaser? :D",
        "I'm supposed to be studying for exam right now.\n So see you maybe like next year after I graduate lol."];
    this.mouseTouchDown = false;
    this.mouseTouchUp = false;
    this.i = 0;
    this.j = 0;
    this.timeCheck = 0;
    this.style = { font: "20px Verdana", fill: "#fff"};


};

BasicGame.Game.prototype = {

    create: function () {

        //  Honestly, just about anything could go here. It's YOUR game after all. Eat your heart out!
        this.timeCheck = this.time.now;
        this.textShow = this.add.text(0, 400, "", this.style);

    },

    update: function () {

        //  Honestly, just about anything could go here. It's YOUR game after all. Eat your heart out!
        if (this.time.now - this.timeCheck > 10)
        {
            if ( this.i < this.dialogue.length){
                if (this.j < this.dialogue[this.i].length){
                    this.timeCheck = this.time.now;
                    this.textShow.text += this.dialogue[this.i][this.j];
                    this.j++;
                }
            }
        //3 seconds have elapsed, so safe to do something
            
            
        }
        else
        {
        //still waiting
        }

        if (this.input.activePointer.isDown) {
            if (!this.mouseTouchDown) {
                this.touchDown();
            };
        } else {
            if (this.mouseTouchDown) {
                this.touchUp();
            };
        }

    },

    touchDown: function(){
        this.mouseTouchDown = true;
        this.i++;
        this.j = 0;
        this.textShow.text = "";
    },

    touchUp: function(){
        this.mouseTouchDown = false;
    },

    quitGame: function (pointer) {

        //  Here you should destroy anything you no longer need.
        //  Stop music, delete sprites, purge caches, free resources, all that good stuff.

        //  Then let's go back to the main menu.
        this.state.start('MainMenu');

    }

};
